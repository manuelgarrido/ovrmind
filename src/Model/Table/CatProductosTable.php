<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class CatProductosTable extends Table {

    public $db_session = "";
    public $event = "";

    public function initialize(array $config) {
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'fecha_creacion' => 'new',
                    'fecha_ultima_modificacion' => 'always'
                ]
            ]
        ]);
        //$this->db_session = new Session();
    }
    
    /*
     * Busca el catalogo por id
     * Fecha: 3 noviembre 2016
     */

    public function getCatalogoById($id) {

        $result = $this->find()->where(['id_producto'=> $id ])->first();
        return $result;
    }     
        
    
    /*
     * Verifica la disponibilidad
     * Fecha: 3 noviembre 2016
     */ 
    public function disponible($data) {

        try {

            //id_producto significa que viene de la pagina de edición
            if (isset($data['id_producto'])) {

                if (strtolower($data['producto']) == strtolower($data['producto_actual'])) {
                    return true;
                }
            }

            $fields = ['producto'];
            $condiciones = ['producto ' => $data['producto']];
            $order = ['id_producto DESC'];

            // Can also be written
            $query = $this->find()
                    ->select($fields)
                    ->where($condiciones)
                    ->order($order);

            if ($query->count() > 0) {
                // si encontro al menos un registro con el nombre igual
                return false;
            } else {
                //no encontro registro alguno
                return true;
            }


            //$log = $this->getDataSource()->getLog(false, false);
            //debug($log);
        } catch (Exception $e) {

            Log::debug($e);
            return (int) false;
        }

    }       

    /*
     * Obtiene la consulta
     * Fecha: 3 noviembre 2016
     */

    public function consulta() {
        
        $condiciones = [];
        
        $fields = ['Inventario.id_producto'];
        
        $joins = [
            ['table' => 'inventario',
                'alias' => 'Inventario',
                'type' => 'LEFT',
                'conditions' => 'CatProductos.id_producto = Inventario.id_tienda'],          
        ];     

        $order = ['CatProductos.id_producto DESC'];

        // Can also be written
        $query = $this->find()
                ->select($fields)
                ->autoFields(true)
                ->join($joins)
                ->order($order)
                ->all();

        return $query;
    }
    
    /*
     * Consulta en forma de lista
     * Fecha: 3 noviembre 2016
     */

    public function lista() {

            
        $fields = ['keyField' => 'id_producto', 'valueField' => 'producto'];
        $condiciones = ['estatus' => 'activo'];
        $order = ['producto'];

        // Can also be written
        $query = $this->find('list', $fields)
                ->where($condiciones)
                ->order($order);

        return $query;
    }           
    
    /*
     * Cambia el estatus
     * Fecha: 3 noviembre 2016
     */

    public function cambiar_estatus($id) {

        $resultado = (int) false;

        try {
            
            //si funciona 
            $contenido = $this->get($id); 
            
            if ($contenido->estatus == "activo"){
                $contenido->estatus = "inactivo";
            }  else {
                $contenido->estatus = "activo";   
            }
            
            if ( $this->save($contenido) ) {
                $resultado = true;
            }            

        } catch (Exception $e) {

            $this->log($e, 'debug');
            return (int) false;
        }

        return $resultado;
    }       
    
        

    
}
