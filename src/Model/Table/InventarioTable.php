<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class InventarioTable extends Table {

    public $db_session = "";
    public $event = "";

    public function initialize(array $config) {
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'fecha_creacion' => 'new',
                    'fecha_ultima_modificacion' => 'always'
                ]
            ]
        ]);
        $this->db_session = new Session();
    }

    /*
     * Obtiene la consulta
     * Fecha: 3 noviembre 2016
     */

   public function consulta($id_tienda = "") {
       
        $conditions = [];

        $fields = ['CatTiendas.tienda', 'Inventario.id_tienda', 'Inventario.id_producto', 'CatProductos.producto', 'Inventario.id_inventario', 'Inventario.cantidad'];
        $joins = [
            ['table' => 'cat_tiendas',
                'alias' => 'CatTiendas',
                'type' => 'INNER',
                'conditions' => 'Inventario.id_tienda = CatTiendas.id_tienda'],
            ['table' => 'cat_productos',
                'alias' => 'CatProductos',
                'type' => 'INNER',
                'conditions' => 'Inventario.id_producto = CatProductos.id_producto']          
        ];
        
        if ($id_tienda != "") {
            $conditions = ['CatTiendas.id_tienda' => $id_tienda, 'CatProductos.estatus' => 'activo'];
        }
        
        $order = ['CatTiendas.tienda'];

        // Can also be written
        $query = $this->find()
                ->select($fields)
                ->join($joins)
                ->where($conditions)
                ->order($order)
                ->all();

        return $query;
    }
    
    /*
     * Verifica la disponibilidad
     * Fecha: 3 noviembre 2016
     */ 
   public function disponibilidad($id_producto = "") {
       
        $conditions = [];

        $fields = ['CatTiendas.tienda', 'CatTiendas.ubicacion', 'CatProductos.producto', 'Inventario.id_inventario', 'Inventario.cantidad'];
        $joins = [
            ['table' => 'cat_tiendas',
                'alias' => 'CatTiendas',
                'type' => 'INNER',
                'conditions' => 'Inventario.id_tienda = CatTiendas.id_tienda'],
            ['table' => 'cat_productos',
                'alias' => 'CatProductos',
                'type' => 'INNER',
                'conditions' => 'Inventario.id_producto = CatProductos.id_producto']          
        ];
        
        $conditions = ['Inventario.id_producto' => $id_producto, 'Inventario.cantidad > 0'];
        
        $order = ['CatTiendas.tienda'];

        // Can also be written
        $query = $this->find()
                ->select($fields)
                ->join($joins)
                ->where($conditions)
                ->order($order)
                ->all();

        return $query;
    }    
   
   
    
    /*
     * Realiza la compra y resta al inventario
     * Fecha: 3 noviembre 2016
     */ 

    public function comprar($data) {
        
        $resultado = (int) false;

        try {
            
            foreach ($data as $key => $item){
                $id_inventario = substr($key, 9); 
                
                $entidad = $this->find()->where(['id_inventario' => $id_inventario ])->first();
                
                if (!empty($entidad)){
                    $cantidad_compra = $entidad->cantidad - $item;
                    //echo $cantidad_compra. "<br />";
                    $data_compra = [];
                    $data_compra['cantidad'] = $cantidad_compra;
                    $entidad = $this->patchEntity($entidad, $data_compra);
                    $this->save($entidad);
                }
            }

            $resultado = true;
        } catch (Exception $e) {

            $this->log($e, 'debug');
            return (int) false;
        }

        return $resultado;
    }            
        

    
}
