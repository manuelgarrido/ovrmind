<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    

    public function initialize(array $config) {
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'fecha_creacion' => 'new',
                    'fecha_ultima_modificacion' => 'always'
                ]
            ]
        ]);
        
    }    

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('username', 'El usuario es requerido')
            ->notEmpty('password', 'La contraseña es requerida')
            ->notEmpty('role', 'A role is required')
            ->add('role', 'inList', [
                'rule' => ['inList', ['admin', 'author']],
                'message' => 'Please enter a valid role'
            ]);
    }

}