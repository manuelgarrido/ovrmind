<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class CatTiendasTable extends Table {

    public $db_session = "";
    public $event = "";

    public function initialize(array $config) {
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'fecha_creacion' => 'new',
                    'fecha_ultima_modificacion' => 'always'
                ]
            ]
        ]);
        $this->db_session = new Session();
    }
    
    /*
     * Busca el catalogo por id
     * Fecha: 3 noviembre 2016
     */

    public function getCatalogoById($id) {

        $result = $this->find()->where(['id_tienda'=> $id ])->first();
        return $result;
    }    
      
    
    /*
     * Verifica la disponibilidad
     * Fecha: 3 noviembre 2016
     */ 
    public function disponible($data) {

        try {

            //id_tienda significa que viene de la pagina de edición
            if (isset($data['id_tienda'])) {

                if (strtolower($data['tienda']) == strtolower($data['tienda_actual'])) {
                    return true;
                }
            }

            $fields = ['tienda'];
            $condiciones = ['tienda ' => $data['tienda']];
            $order = ['id_tienda DESC'];

            // Can also be written
            $query = $this->find()
                    ->select($fields)
                    ->where($condiciones)
                    ->order($order);

            if ($query->count() > 0) {
                // si encontro al menos un registro con el nombre igual
                return false;
            } else {
                //no encontro registro alguno
                return true;
            }


            //$log = $this->getDataSource()->getLog(false, false);
            //debug($log);
        } catch (Exception $e) {

            Log::debug($e);
            return (int) false;
        }

    }       

    /*
     * Obtiene la consulta
     * Fecha: 20 noviembre 2015
     */

    public function consulta() {
        
        $condiciones = [];
        
        $fields = ['Inventario.id_tienda'];
        
        $joins = [
            ['table' => 'inventario',
                'alias' => 'Inventario',
                'type' => 'LEFT',
                'conditions' => 'CatTiendas.id_tienda = Inventario.id_tienda'],          
        ];     

        $order = ['CatTiendas.id_tienda DESC'];

        // Can also be written
        $query = $this->find()
                ->select($fields)
                ->autoFields(true)
                ->join($joins)
                ->order($order)
                ->all();

        return $query;
    }
    
    /*
     * Consulta en forma de lista
     * Fecha: 3 noviembre 2016
     */

    public function lista() {

            
        $fields = ['keyField' => 'id_tienda', 'valueField' => 'tienda'];
        $condiciones = ['estatus' => 'activo'];
        $order = ['tienda'];

        // Can also be written
        $query = $this->find('list', $fields)
                ->where($condiciones)
                ->order($order);

        return $query;
    }           
    
    /*
     * Cambia el estatus
     * Fecha: 3 noviembre 2016
     */

    public function cambiar_estatus($id) {

        $resultado = (int) false;

        try {
            
            //si funciona 
            $contenido = $this->get($id); 
            
            if ($contenido->estatus == "activo"){
                $contenido->estatus = "inactivo";
            }  else {
                $contenido->estatus = "activo";   
            }
            
            if ( $this->save($contenido) ) {
                $resultado = true;
            }            

        } catch (Exception $e) {

            $this->log($e, 'debug');
            return (int) false;
        }

        return $resultado;
    } 
      
    
    
}
