<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// in src/Form/ContactForm.php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class InventarioForm extends Form {

     /* 
     * Valida los campos del formulario 
     * Fecha: 7 abril 2017
     */      
    protected function _buildValidator(Validator $validator) {
        
        //Indicamos que los siguientes campos son:
        //Obligatorios
        return $validator
                        ->requirePresence('id_producto')
                        ->requirePresence('id_tienda')
                        ->requirePresence('cantidad');
    }

     /* 
     * Retorna unicamente true en caso de que la validacíon este correcta
     * Fecha: 7 abril 2017
     */        
    protected function _execute(array $data) {
        return true;
    }

}
