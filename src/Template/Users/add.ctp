    <div class="container">
        <div class="card card-container">
            <img width="270" src="<?= $this->Url->build('/', true)."img/ovrmind_logo.jpg" ?>" />
            <p id="profile-name" class="profile-name-card"></p>
            <!-- Form -->
            <?= $this->Flash->render('auth') ?>
            <?php echo $this->Form->create($user, array('name' => 'validar_formulario', 'id' => 'validar_formulario', 'class' => 'form-signin' ) ); ?>
                <div class="text-right">
                    <a href="<?= $this->Url->build('/', true); ?>/users/login">Acceso</a>   
                </div>                
                <span id="reauth-email" class="reauth-email"></span>
                <input type="text" name="username" id="inputEmail" class="form-control" placeholder="Usuario" required autofocus>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Contraseña" required>
                
                <?= $this->Form->input('role', ['id' => 'inputEmail',
                    'options' => ['admin' => 'Admin']
                ]) ?>
                
                <input type="text" name="nombre" id="inputEmail" class="form-control" placeholder="Nombre" required autofocus>
                <input type="text" name="apellidos" id="inputEmail" class="form-control" placeholder="Apellidos" required autofocus>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Registrar</button>
            <?php echo $this->Form->end(); ?> 
            <!-- Termina form -->   
        </div>
    </div><!-- /container -->