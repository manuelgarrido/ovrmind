<!-- Form -->
<?php echo $this->element('menu'); ?>
<?php echo $this->Form->create('Inventario', array('name' => 'formulario_principal', 'id' => 'formulario_principal')); ?> 
<input type="hidden" name="id" id="id" />
<input type="hidden" name="nombre" id="nombre" />   
<div class="col-md-12">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                <!--<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>-->
            </div>

            <h2 class="panel-title">Inventario</h2>
            <a href="<?php echo $this->Url->build('/', true) . "inventario/agregar"; ?>">Agregar</a>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover mb-none">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tienda</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($inventario->count() == 0) { ?>
                            <tr><td colspan="7"><div align="center">No hay inventario.</div></td></tr>                              
                        <?php } ?>
                        <?php foreach ($inventario as $row) {
                            ?>                        
                            <tr>
                                <td><?php echo $row->id_inventario; ?></td>
                                <td><a href="<?php echo $this->Url->build('/', true); ?>inventario/editar/<?php echo $row->id_inventario; ?>" data-toggle="tooltip"><?php echo $row->CatTiendas['tienda']; ?></a></td>
                                <td><a href="<?php echo $this->Url->build('/', true); ?>inventario/editar/<?php echo $row->id_inventario; ?>" data-toggle="tooltip"><?php echo $row->CatProductos['producto']; ?></a></td>
                                <td><?php echo $row->cantidad; ?></td>
                                <td class="actions-hover actions-fade">
                                    <a href="<?php echo $this->Url->build('/', true); ?>inventario/editar/<?php echo $row->id_inventario; ?>"><i class="fa fa-pencil"></i></a>
                                    
                                    <!-- Ventana modal para eliminar  -->

                                    <button type="button" class="btn btn-info btn-default" data-toggle="modal" data-target="#modalEliminar_<?php echo $row->id_inventario; ?>">Eliminar</button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="modalEliminar_<?php echo $row->id_inventario; ?>" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Modal Header</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <h4>Eliminar éste registro del inventario</h4>
                                                        <p>¿Estas seguro(a) de eliminar el elemento(a) <b><?php echo $row->CatProductos['producto']; ?></b>?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary modal-confirm" onclick="eliminar(<?php echo $row->id_inventario; ?>)">Confirmar</button>
                                                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- Termina Ventana modal para eliminar  -->                                          
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<?php echo $this->Form->end(); ?> 
<!-- /Form -->

<script>
    function activar(id, nombre) {
        $('#id').val(id);
        

        document.forms["formulario_principal"].action = "<?php echo $this->Url->build('/', true); ?>inventario/activar";
        document.forms["formulario_principal"].submit();
    }
    
    function eliminar(id, nombre) {
        $('#id').val(id);
        

        document.forms["formulario_principal"].action = "<?php echo $this->Url->build('/', true); ?>inventario/eliminar";
        document.forms["formulario_principal"].submit();
    }    
</script>