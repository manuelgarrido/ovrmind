<style type="text/css">
	.navbar{
		margin-top: 20px;
	}
</style>

<div class="container">
    <nav role="navigation" class="navbar navbar-inverse">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo $this->Url->build('/', true); ?>" class="navbar-brand">Ovrmind</a>
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo $this->Url->build('/', true); ?>inventario">Inventario</a></li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Catálogos <b class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="<?php echo $this->Url->build('/', true); ?>catalogos/consulta/tiendas">Tiendas</a></li>
                        <li><a href="<?php echo $this->Url->build('/', true); ?>catalogos/consulta/productos">Productos</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo $this->Url->build('/', true); ?>/users/logout">Salir</a></li>
            </ul>
        </div>
    </nav>
</div>