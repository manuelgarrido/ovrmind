<!-- Form -->
<?php echo $this->element('menu'); ?>
<?php echo $this->Form->create($validationForm, ['class' => 'form-horizontal form-bordered', 'id' => 'validar_formulario'] ); ?>  
<?php echo $this->Form->hidden('tienda_actual', array('value' => $this->request->data['tienda'])) ?>

<!-- alta de catalogo -->
<!-- renombrado por conflicto con css -->
<div class="row2"> 
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading"> 
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <!--<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>-->
                </div>

                <h2 class="panel-title">Editar tienda
                <?php echo $this->Html->link(__('Regresar', true), ['action'=>'consulta/'.$catalogo], ['class'=>'btn btn-default']);?>                
                </h2>
            </header>
            <div class="panel-body">
                <!-- empieza formulario -->

                <div class="form-group">
                    <label class="col-md-3 control-label" for="tienda">Tienda</label>
                    <div class="col-md-6">  
                        <?php echo $this->Form->hidden('id_tienda'); ?>
                        <?php
                        echo $this->Form->input('tienda', array(
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => 'Escribe una tienda',
                            'maxlength' => 50,
                            'onkeypress' => 'return soloLetrasyNumeros(event)',
                            'onpaste' => 'return false'
                        ));
                        ?>                            
                    </div>
                </div>   
                
                <div class="form-group">
                    <label class="col-md-3 control-label" for="tienda">Ubicación</label>
                    <div class="col-md-6">                      
                        <?php
                        echo $this->Form->input('ubicacion', array(
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => 'Escribe una ubicación',
                            'maxlength' => 50,
                            'onkeypress' => 'return soloLetrasyNumeros(event)',
                            'onpaste' => 'return false'
                        ));
                        ?>                            
                    </div>
                </div>                        

                <div class="form-group">
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->button('Guardar', array(
                            'type' => 'submit',
                            'class' => 'mb-xs mt-xs mr-xs btn btn-default',
                            'escape' => true
                        ));
                        ?> 
                    </div>                        
                </div>                      


                <!-- acaba formulario -->

            </div>
        </section>


    </div>
</div>
<!-- Termina alta de catalogo -->

<?php echo $this->Form->end(); ?> 
<!-- Termina form -->

<script>
document.getElementById("tienda").focus();
</script>