<!-- Form -->
<?php echo $this->element('menu'); ?>
<?php echo $this->Form->create('Catalogos', array('name' => 'formulario_principal', 'id' => 'formulario_principal')); ?> 
<input type="hidden" name="id" id="id" />
<input type="hidden" name="nombre" id="nombre" />   
<div class="col-md-12">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                <!--<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>-->
            </div>

            <h2 class="panel-title">Catálogo de <?= $catalogo ?></h2>
            <a href="<?php echo $this->Url->build('/', true) . "catalogos/agregar/" . $catalogo; ?>">Agregar</a>
        </header>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover mb-none">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tienda</th>
                            <th>Inventario</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($cat->count() == 0) { ?>
                            <tr><td colspan="7"><div align="center">No hay tiendas.</div></td></tr>                              
                        <?php } ?>
                        <?php foreach ($cat as $row) {
                            ?>                        
                            <tr>
                                <td><?php echo $row->id_tienda; ?></td>
                                <td><a href="<?php echo $this->Url->build('/', true); ?>catalogos/editar/tiendas/<?php echo $row->id_tienda; ?>" data-toggle="tooltip"><?php echo $row->tienda; ?></a></td>
                                <td><?php echo ( !is_null($row->Inventario['id_tienda']) ) ? "Si" : "No"; ?></td>
                                <td>
                                    <?php
                                    if ($row->estatus == "activo") {
                                        echo "Activo <a href='javascript:;' onclick='activar(\"" . $row->id_tienda . "\")'>(Desactivar)</a>";
                                    } else {
                                        echo "Desactivado <a href='javascript:;' onclick='activar(\"" . $row->id_tienda . "\")'>(Activar)</a>";
                                    }
                                    ?>
                                </td>
                                <td class="actions-hover actions-fade">
                                    <a href="<?php echo $this->Url->build('/', true); ?>catalogos/editar/tiendas/<?php echo $row->id_tienda; ?>"><i class="fa fa-pencil"></i></a>
                                    
                                    <!-- Ventana modal para eliminar  -->
                                    <?php if ( is_null($row->Inventario['id_tienda']) ){ ?>
                                    <button type="button" class="btn btn-info btn-default" data-toggle="modal" data-target="#modalEliminar_<?php echo $row->id_tienda; ?>">Eliminar</button>
                                    <?php }else {
                                        echo "Inventariado, no se puede eliminar";
                                    } ?>
                                    <!-- Modal -->
                                    <div class="modal fade" id="modalEliminar_<?php echo $row->id_tienda; ?>" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Modal Header</h4>
                                                </div>
                                                <div class="modal-body">
                                                        <h4>Eliminar <?= $catalogo ?></h4>
                                                        <p>¿Estas seguro(a) de eliminar el elemento(a) <b><?php echo $row->producto; ?></b>?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-primary modal-confirm" onclick="eliminar(<?php echo $row->id_tienda; ?>)">Confirmar</button>
                                                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- Termina Ventana modal para eliminar  -->                                    
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<?php echo $this->Form->end(); ?> 
<!-- /Form -->

<script>
    function activar(id, nombre) {
        $('#id').val(id);
        

        document.forms["formulario_principal"].action = "<?php echo $this->Url->build('/', true); ?>catalogos/activar/<?= $catalogo; ?>";
        document.forms["formulario_principal"].submit();
    }
    
    function eliminar(id, nombre) {
        $('#id').val(id);
        

        document.forms["formulario_principal"].action = "<?php echo $this->Url->build('/', true); ?>catalogos/eliminar/<?= $catalogo; ?>";
        document.forms["formulario_principal"].submit();
    }    
</script>