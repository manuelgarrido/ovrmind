<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Form\CatTiendasForm;
use App\Form\CatProductosForm;

class CatalogosController extends AppController {

    public function initialize() {
        parent::initialize();
    }

     /* 
     * Pagina por default 
     * Fecha: 7 abril 2017
     */
    
    public function index() {
        
    }

     /* 
     * Consulta de catálogos
     * Fecha: 7 abril 2017
     */    
    public function consulta($catalogo) {

        $modelo = 'Cat' . ucwords($catalogo);
        //Carga el modelo correspondiente de manera dinámica
        $this->loadModel($modelo);

        //Setea las variable para las vistas
        $this->set('catalogo', $catalogo);
        $this->set('titulo', $catalogo);
        $this->set('cat', $this->$modelo->consulta());

        $this->render($catalogo . "/index");
    }

    /*
     * Agrega elementos al catalogo seleccionado
     * Fecha: 7 abril 2017
     */

    public function agregar($catalogo) {

        $modelo = 'Cat' . ucwords($catalogo);

        $this->$modelo = TableRegistry::get($modelo);

        $this->set('catalogo', $catalogo);

        //Consigue el form de validación correcto para la validación
        $validationForm = $this->getForm($catalogo);

        if ($catalogo == "especialistas") {

            $usuarios_disponibles = $this->CatEspecialistas->usuarios_disponibles();
            //print_r($usuarios_merged);
            $this->set('usuarios_disponibles', $usuarios_disponibles);
        }

        //Comprueba que las siguientes operaciones sean post o put
        if ($this->request->is('post') || $this->request->is('put')) {
      

            $contenido = $this->$modelo->newEntity($this->request->data);

            //empieza if de validationForm
            if ($validationForm->execute($this->request->data)) {

                //Si existe regresa true, si no existe regresa false
                if ($this->$modelo->disponible($this->request->data)) {

                    $contenido = $this->$modelo->patchEntity($contenido, $this->request->data);

                    //Guarda los datos del formulario 
                    if ($this->$modelo->save($contenido)) {

                        $this->Flash->success('Ha sido agregado', [
                            'key' => 'success',
                            'params' => array()
                        ]);


                        return $this->redirect(['action' => 'consulta/' . $catalogo]);
                    } else {

                        $this->Flash->fail('No se pudo ser agregado', [
                            'key' => 'fail',
                            'params' => array()
                        ]);
                    }
                } else {

                    $this->Flash->fail('Ya existe éste nombre, para poder usar este nombre:', [
                        'key' => 'fail',
                        'params' => []
                    ]);
                }


                //termina if de validationForm
            }
        }

        $this->set('validationForm', $validationForm);
        $this->render($catalogo . "/agregar");
    }

    /*
     * Edita un catalogo
     * Fecha: 7 abril 2017
     */

    public function editar($catalogo, $id = NULL) {

        $modelo = 'Cat' . ucwords($catalogo);

        $this->$modelo = TableRegistry::get($modelo);

        //Consigue el form de validación correcto para la validación
        $validationForm = $this->getForm($catalogo);

        $this->set('catalogo', $catalogo);
        $this->set('cat', $this->$modelo->consulta());

        //Comprueba que las siguientes operaciones sean post o put
        if ($this->request->is('post') || $this->request->is('put')) {

            $contenido = $this->$modelo->newEntity($this->request->data);

            //empieza if de validationForm
            if ($validationForm->execute($this->request->data)) {
                //Verifica si el nombre de cuenta, ya existe o no
                //Si existe regresa true, si no existe regresa false
                if ($this->$modelo->disponible($this->request->data)) {

                    $contenido = $this->$modelo->patchEntity($contenido, $this->request->data);

                    //Guarda los datos del formulario
                    if ($this->$modelo->save($contenido)) {

                        $this->Flash->success('Tus datos han sido actualizados', [
                            'key' => 'success',
                            'params' => array()
                        ]);

                        return $this->redirect(['action' => 'consulta/' . $catalogo]);
                    } else {

                        $this->Flash->fail('No se pudo actualizar tus datos', [
                            'key' => 'fail',
                            'params' => array()
                        ]);
                    }
                } else {

                    $this->Flash->fail('Ya existe esta información con este nombre o descripción:', [
                        'key' => 'fail',
                        'params' => []
                    ]);
                }
                //termina if de validationForm
            }
        } else {

            $this->request->data = $this->$modelo->getCatalogoById($id);

            //Si la cuenta no existe
            if (empty($this->request->data)) {
                $this->Flash->fail('La información a la que intentas acceder, no existe', [
                    'key' => 'fail',
                    'params' => array()
                ]);
                return $this->redirect(array('action' => 'consulta/' . $catalogo));
            }
        }

        $this->set('validationForm', $validationForm);
        $this->render($catalogo . "/editar");
    }

    /*
     * Elimina un elemento de un catalogo
     * Fecha: 7 abril 2017
     */

    public function activar($catalogo) {

        $modelo = 'Cat' . ucwords($catalogo);

        $this->$modelo = TableRegistry::get($modelo);

        $this->no_get();

        //Comprueba que las siguientes operaciones sean post o put
        if ($this->request->is('post') || $this->request->is('put')) {

            $id = $this->request->data['id'];

            //$entity = $this->$modelo->get($id);
            //$this->$modelo->delete($entity)

            if ($this->$modelo->cambiar_estatus($id)) {

                $this->Flash->success('El estatus ha sido cambiado', [
                    'key' => 'success',
                    'params' => array()
                ]);
            } else {

                $this->Flash->fail('El estatus no pudo ser cambiado', [
                    'key' => 'fail',
                    'params' => array()
                ]);
            }
        }

        $regreso = $this->request->data('regreso');
        return $this->redirect(array('action' => 'consulta/' . $catalogo));
    }

    /*
     * Elimina un elemento de un catalogo
     * Fecha: 7 abril 2017
     */

    public function eliminar($catalogo) {

        $modelo = 'Cat' . ucwords($catalogo);

        $this->$modelo = TableRegistry::get($modelo);

        //Obliga que no sea por el metodo get
        $this->no_get();

        //Comprueba que las siguientes operaciones sean post o put
        if ($this->request->is('post') || $this->request->is('put')) {

            $id = $this->request->data['id'];

            $entity = $this->$modelo->get($id);


            if ($this->$modelo->delete($entity)) {

                $this->Flash->success('El elemento del catalogo ha sido eliminado', [
                    'key' => 'success',
                    'params' => array()
                ]);
            } else {

                $this->Flash->fail('El elemento del catalogo no pudo ser eliminado', [
                    'key' => 'fail',
                    'params' => array()
                ]);
            }
        }

        $regreso = $this->request->data('regreso');
        return $this->redirect(array('action' => 'consulta/' . $catalogo));
    }

     /* 
     * Carga dinamicamente validador form por cada catálogo diferente
     * Fecha: 7 abril 2017
     */
    public function getForm($catalogo) {
        switch ($catalogo) {
            case "tiendas":
                return new CatTiendasForm();
                break;
            case "productos":
                return new CatProductosForm();
                break;
                        
            default:
                break;
        }
    }
    
    

}
