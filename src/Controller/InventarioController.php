<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Controller\AppController;
use App\Form\InventarioForm;
use Cake\ORM\TableRegistry;

class InventarioController extends AppController {

    
     /* 
     * Consulta del inventario
     * Fecha: 7 abril 2017
     */    

    public function index() {

        $this->set('inventario', $this->Inventario->consulta());
    }

    /*
     * Agrega una inventario
     * Fecha: 7 abril 2017
     */

    public function agregar() {

        $this->cargarCatalogos();

        $validationForm = new InventarioForm();

        if ($this->request->is('post') || $this->request->is('put')) {

            //empieza if de validationForm
            if ($validationForm->execute($this->request->data)) {

                $entidad = $this->Inventario->newEntity($this->request->data);
                
                 $duplicado = $this->Inventario->find()->where(['id_tienda'=> $this->request->data['id_tienda'], 'id_producto' => $this->request->data['id_producto'] ])->first();
                 
                 if (empty($duplicado)){
                     
                    //Guarda los datos del formulario
                    if ($this->Inventario->save($entidad)) {

                        $this->Flash->success('Tu inventario ha sido agregado', [
                            'key' => 'success',
                            'params' => array()
                        ]);

                        return $this->redirect(['action' => 'index']);
                    } else {

                        $this->Flash->fail('No se pudo agregar al inventario', [
                            'key' => 'fail',
                            'params' => array()
                        ]);
                    }                     
                     
                 }else {
                    $this->Flash->fail('Este inventario ya se encuentra, cambia la tienda o el producto', [
                        'key' => 'fail',
                        'params' => array()
                    ]);
                 }
               
            }
        }

        $this->set('validationForm', $validationForm);
    }

    /*
     * Edita una inventario
     * Fecha: 7 abril 2017
     */

    public function editar($id = null) {

        $this->cargarCatalogos();
        $validationForm = new InventarioForm();

        if (!$id) {

            $this->Flash->fail('Error, inventario no válida', [
                'key' => 'fail',
                'params' => array()
            ]);
            return $this->redirect(array('action' => 'index'));
        }


        //guradado de datos
        if ($this->request->is('post') || $this->request->is('put')) {

            //empieza if de validationForm
            if ($validationForm->execute($this->request->data)) {


                $entidad = $this->Inventario->find()->where(['id_inventario' => $this->request->data['id_inventario']])->first();

                if (!empty($entidad)) {

                    $entidad = $this->Inventario->patchEntity($entidad, $this->request->data);

                    //Guarda los datos del formulario
                    if ($this->Inventario->save($entidad)) {

                        $this->Flash->success('Tu inventario ha sido actualizado', [
                            'key' => 'success',
                            'params' => array()
                        ]);

                        return $this->redirect(['action' => 'index']);
                    } else {

                        $this->Flash->fail('No se pudo actualizar el inventario', [
                            'key' => 'fail',
                            'params' => array()
                        ]);
                    }
                } else {

                    $this->Flash->fail('Inventario inválida o alterado', [
                        'key' => 'fail',
                        'params' => array()
                    ]);
                    return $this->redirect(['action' => 'index']);
                }
            }
        } else {
            //carga de datos
            $this->request->data = $this->Inventario->find()->where(['id_inventario' => $id])->first();

            //Si la inventario no existe
            if (empty($this->request->data)) {
                $this->Flash->fail('La inventario al que intentas acceder, no existe', [
                    'key' => 'fail',
                    'params' => array()
                ]);
                return $this->redirect(array('action' => 'index'));
            }
        }

        $this->set('validationForm', $validationForm);
    }

    /*
     * Elimina una inventario
     * Fecha: 7 abril 2017
     */

    public function eliminar() {

        $this->no_get();

        if ($this->request->is('post') || $this->request->is('put')) {

            $id = $this->request->data('id');
            $nombre = $this->request->data('nombre');

            $entity = $this->Inventario->get($id);

            if ($this->Inventario->delete($entity)) {

                $this->Flash->success('La inventario ' . $nombre . ' con id ' . $id . ' ha sido eliminado', [
                    'key' => 'success',
                    'params' => array()
                ]);
            } else {

                $this->Flash->fail('La inventario ' . $nombre . 'con id ' . $id . ' no pudo ser eliminado', [
                    'key' => 'fail',
                    'params' => array()
                ]);
            }
        }

        return $this->redirect(array('action' => 'index'));
    }

    /*
     * Carga todos los catalogos necesarios para este controlador
     * Fecha: 7 abril 2017
     */

    public function cargarCatalogos() {

        $this->CatProductos = TableRegistry::get("CatProductos");
        $this->set('cat_productos', $this->CatProductos->lista());

        $this->CatTiendas = TableRegistry::get("CatTiendas");
        $this->set('cat_tiendas', $this->CatTiendas->lista());
    }

}
